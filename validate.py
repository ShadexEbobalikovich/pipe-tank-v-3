import os
import sys

from asn1crypto.cms import ContentInfo

unpack = 'openssl cms -verify -in {input} -inform DER -noverify -outform DER -out {output}'


def load():
    iterations, filename = sys.argv[1].split('_')
    return int(iterations), filename


def get_filename(iterations, filename):
    return f'{iterations}_{filename}' if iterations > 0 else filename.replace('.p7s', '')


def read_signature(iterations, filename) -> bytes:
    with open(get_filename(iterations, filename), 'rb') as inp:
        return inp.read()


def parse_issuer(signature):
    content: ContentInfo = ContentInfo.load(signature)
    name = content.native['content']['signer_infos'][0]['sid']['issuer']['common_name']
    print('Issuer:', name)


def unpack_signature(iterations, filename):
    source = get_filename(iterations, filename)
    dist = get_filename(iterations - 1, filename)
    os.system(unpack.format(input=source, output=dist))
    os.remove(source)


def main():
    iterations, filename = load()
    for i in range(iterations, 0, -1):
        signature = read_signature(i, filename)
        parse_issuer(signature)
        unpack_signature(i, filename)


if __name__ == '__main__':
    main()
